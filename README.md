# Coursera "Разработка веб-сервисов на Go - основы языка"

## Лекция 1: Введение в Golang

### Конспект к лекции 

https://drive.google.com/open?id=1y48LQg60ryecN5bzwYlZIT1tb9Qie_Cp

### Код к лекции

https://drive.google.com/open?id=1pjs4O1lkJr5vYySpg_LYz_Lzv4w3RSB3
    
             Код необходимо распаковать в папку $GOPATH/src/coursera

             После распаковки у вас будет папка $GOPATH/src/coursera/basics

             Например, /home/rvasily/go/src/coursera/basics

### Задание к лекции
https://drive.google.com/open?id=1W5PaS-NgnR0KpM-mcERkqZzJVqwlVwDf

### Описание лекции

В этом занятии мы разберёмся с самыми основами языка. Несмотря то, что во всех языках это плюс-минус одинаково - в go есть свои нюансы, которые надо знать. Впереди нас ждут такие удивительные вещи, как переменные, управляющие структуры, функции, структуры с методами и интерфейсы.

### Материалы к лекции

Материалы для дополнительного чтения на английском:

1. https://golang.org/ref/spec - спецификация по язык
2. https://golang.org/ref/mem - модель памяти го. на начальном этапе не надо, но знать полезно
3. https://golang.org/doc/code.html - про организацию кода. GOPATH и пакеты
4. https://golang.org/cmd/go/
5. https://blog.golang.org/strings
6. https://blog.golang.org/slices
7. https://blog.golang.org/go-slices-usage-and-internals
8. https://github.com/golang/go/wiki - вики го на гитхабе. очень много полезной информации
9. https://blog.golang.org/go-maps-in-action
10. https://blog.golang.org/organizing-go-code
11. https://golang.org/doc/effective_go.html - основной сборник тайного знания, сюда вы будуте обращатсья в первое время часто
12. https://github.com/golang/go/wiki/CodeReviewComments как ревьювить (и писать код). обязательно к прочтению
13. https://divan.github.io/posts/avoid_gotchas/ - материал аналогичный 50 оттенков го
14. https://research.swtch.com/interfaces
15. https://research.swtch.com/godata
16. http://jordanorelli.com/post/42369331748/function-types-in-go-golang
17. https://www.devdungeon.com/content/working-files-go - работа с файлами
18. http://www.golangprograms.com - много how-to касательно базовых вещей в go
19. http://yourbasic.org/golang/ - ещё большой набор how-to где можно получить углублённую информацию по всем базовым вещам. очень полезны http://yourbasic.org/golang/blueprint/
20. https://github.com/Workiva/go-datastructures
21. https://github.com/enocom/gopher-reading-list - большая подборка статей по многим темам ( не только данной лекции )
22. https://www.youtube.com/watch?v=MzTcsI6tn-0 - как организовать код
23. https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1 - статья на предыдущую тему

Материалы для дополнительного чтения на русском:

1. https://habrahabr.ru/company/mailru/blog/314804/ - 50 оттенков го. обязательно к прочтению. многое оттуда мы ещё не проходили, но на будущее - имейте ввиду
2. https://habrahabr.ru/post/306914/ - Разбираемся в Go: пакет io
3. https://habrahabr.ru/post/272383/ - постулаты go. Маленькая статья об основными принципах языка
4. https://habrahabr.ru/company/mailru/blog/301036/ - лучшие практики go
5. https://habrahabr.ru/post/308198/ - организация кода в go
6. https://habrahabr.ru/post/339192/ - Зачем в Go амперсанд и звёздочка (& и *)
7. https://habrahabr.ru/post/325468/ - как не наступать на грабли в Go
8. https://habrahabr.ru/post/276981/ - Краш-курс по интерфейсам в Go
9. http://golang-book.ru

Литература по го на русском языке:

1. Язык программирования Go, Алан А. А. Донован, Брайан У. Керниган
2. Go на практике, Matt Butcher, Мэтт Фарина Мэтт
3. Программирование на Go. Разработка приложений XXI века, Марк Саммерфильд

Дополнительные упражнения:

1. https://go-tour-ru-ru.appspot.com/list - упражнения на овладение базовым синтаксисом, на случай если вам нужна небольшая практика перед первым заданием курса

## Лекция 2: Асинхронная работа

### Конспект к лекции 

https://drive.google.com/open?id=1sRYxDVaqIOj_pg-HYLlS4OOMkXhVop6V

### Код к лекции

https://drive.google.com/open?id=1Gg0VB1YRp-d0S5RJ_InFgJPKc08-MuCX
    
             Код необходимо распаковать в папку $GOPATH/src/coursera

             После распаковки у вас будет папка $GOPATH/src/coursera/basics

             Например, /home/rvasily/go/src/coursera/basics

### Задание к лекции
https://drive.google.com/open?id=1G1XQuuM7TIQZFTVQxttS5qCn7DXkHP12

### Описание лекции

Одно из самых важных занятий на курсе - асинхрон и конкурентность. Главная особенность языка, которая в сумме с его простотой синтаксиса дают такой мощный эффект. Уделите особое внимание тому занятию.

### Материалы к лекции

На английском:

1. https://blog.golang.org/race-detector
2. https://blog.golang.org/pipelines
3. https://blog.golang.org/advanced-go-concurrency-patterns
4. https://blog.golang.org/go-concurrency-patterns-timing-out-and
5. https://talks.golang.org/2012/concurrency.slide#1
6. https://www.goinggo.net/2017/10/the-behavior-of-channels.html
7. http://marcio.io/2015/07/handling-1-million-requests-per-minute-with-golang/ - рассказ про оптимизацию воркер пула
8. http://www.tapirgames.com/blog/golang-channel
9. http://www.tapirgames.com/blog/golang-channel-closing
10. https://github.com/golang/go/wiki/CommonMistakes

Видео:

1. https://www.youtube.com/watch?v=5buaPyJ0XeQ - классное выступление Dave Cheney про функции первого класса и использование их с горутинами, очень рекомендую, оно небольшое
2. https://www.youtube.com/watch?v=f6kdp27TYZs - Google I/O 2012 - Go Concurrency Patterns - очень рекомендую
3. https://www.youtube.com/watch?v=rDRa23k70CU&list=PLDWZ5uzn69eyM81omhIZLzvRhTOXvpeX9&index=15 - ещё одно хорошее видео про паттерны конкуренции в го
4. https://www.youtube.com/watch?v=KAWeC9evbGM - видео Андрея Смирнова с конференции Highload - в нём вы можете получить более детальную информацию по теме вводного видео (методы обработки запросов и плюсы неблокирующего подхода), о том, что там творится на системном уровне. На русском, не про go

На русском:

1. https://habrahabr.ru/post/141853/ - как работают горутины
2. https://habrahabr.ru/post/308070/ - как работают каналы
3. https://habrahabr.ru/post/333654/ - как работает планировщик ( https://rakyll.org/scheduler/ )
4. https://habrahabr.ru/post/271789/ - танцы с мютексами

Книги:

1. Язык программирования Go, Алан А. А. Донован, Брайан У. Керниган - глава 8
2. Concurrency in Go: Tools and Techniques for Developers, by Katherine Cox-Buday

## Лекция 3:  Работа с динамическими данными и производительность

### Конспект к лекции 

https://drive.google.com/open?id=13wtNK8YIMzqS4PDmmlAHLcrOs5BMBIci

### Код к лекции

https://drive.google.com/open?id=1a5FXR3QRnk7KBvXFSDK3P8G2Qs7fbmAT

    
             Код необходимо распаковать в папку $GOPATH/src/coursera

             После распаковки у вас будет папка $GOPATH/src/coursera/basics

             Например, /home/rvasily/go/src/coursera/basics

### Задание к лекции

https://drive.google.com/open?id=1PlMQmXCC6q74fE_lQR0YJMBCb2sOY-ml

### Описание лекции

Дальнейшие работа будет затруднительна без понимания, каким образом в go обращаться с динамическими данными. Поэтому эту лекцию мы начнём с распаковки-запаковки JSON, а далее рассмотрим что лежит под капотом - рефлексию и кодогенерацию, а так же какой из этих подходов быстрее и как это определить:

### Материалы к лекции

Рефлексия и кодогенерация:

1. https://blog.golang.org/laws-of-reflection
2. https://habrahabr.ru/post/269887/
3. https://golang.org/src/go/ast/example_test.go
4. https://github.com/golang/tools/blob/master/cmd/stringer/stringer.go
5. https://golang.org/pkg/reflect/
6. http://blog.burntsushi.net/type-parametric-functions-golang/
7. https://habrahabr.ru/post/269887/
8. https://medium.com/kokster/go-reflection-creating-objects-from-types-part-i-primitive-types-6119e3737f5d
9. https://medium.com/kokster/go-reflection-creating-objects-from-types-part-ii-composite-types-69a0e8134f20

Производительность:

Материалы на русском:

1. https://habrahabr.ru/company/badoo/blog/301990/
2. https://habrahabr.ru/company/badoo/blog/324682/
3. https://habrahabr.ru/company/badoo/blog/332636/
4. https://habrahabr.ru/company/mailru/blog/331784/ - статья про то как Почта@Mail.ru держит 3 миллиона вебсокет-соединений

Материалы на английском:

1. https://blog.golang.org/profiling-go-programs
2. https://about.sourcegraph.com/go/an-introduction-to-go-tool-trace-rhys-hiltner/ - большая статья, посвященная go tool trace
3. https://www.goinggo.net/2017/05/language-mechanics-on-stacks-and-pointers.html
4. https://www.rzaluska.com/blog/important-go-interfaces/
5. https://docs.google.com/document/d/1CxgUBPlx9iJzkz9JWkb6tIpTe5q32QDmz8l0BouG0Cw/preview
6. https://segment.com/blog/allocation-efficiency-in-high-performance-go-services/
7. https://lwn.net/Articles/250967/ - не про го, но знать полезно
8. https://github.com/golang/go/wiki/Performance - много про то что можно вытащить из pprof-а
9. https://golang.org/doc/gdb
10. https://about.sourcegraph.com/go/advanced-testing-in-go/
11. https://about.sourcegraph.com/go/generating-better-machine-code-with-ssa/
12. https://about.sourcegraph.com/go/evolutionary-optimization-peter-bourgon/
13. https://signalfx.com/blog/a-pattern-for-optimizing-go-2/
14. http://go-talks.appspot.com/github.com/davecheney/presentations/performance-without-the-event-loop.slide#1
15. https://dave.cheney.net/2013/06/30/how-to-write-benchmarks-in-go
16. https://dave.cheney.net/2014/06/07/five-things-that-make-go-fast - вообще в блоге Дейва очень много полезной инфы по го
17. https://github.com/dgryski/go-perfbook/blob/master/performance.md
18. https://www.youtube.com/watch?v=NS1hmEWv4Ac - Make your Go go faster! Optimising performance through reducing memory allocations + слайды https://fosdem.org/2018/schedule/event/faster/attachments/slides/2510/export/events/attachments/faster/slides/2510/BryanBorehamGoOptimisation.pdf
19. https://www.youtube.com/watch?v=N3PWzBeLX2M - Profiling and Optimizing Go
20. https://www.youtube.com/watch?v=Lxt8Vqn4JiQ - Golang UK Conference 2017 | Filippo Valsorda - Fighting latency: the CPU profiler is not your ally
21. https://www.youtube.com/watch?v=ydWFpcoYraU - Finding Memory Leaks in Go Programs
22. http://www.integralist.co.uk/posts/profiling-go/
23. https://bravenewgeek.com/so-you-wanna-go-fast/

Тесты:

1. https://blog.golang.org/cover - расширенная информация о go test -cover

Полезные инструменты:

1. https://mholt.github.io/json-to-go - позволяет по json сформировать структуру на go, в которую он может быть распакован
2. https://github.com/mailru/easyjson - кодогенератор для json от mail.ru

## Лекция 4: Основы HTTP

### Конспект к лекции 

https://drive.google.com/open?id=1FQO5ZYsQXPFDHjA4r_hT0Zl8S9LuMOPW

### Код к лекции

https://drive.google.com/open?id=1HsCTpWDCFYMYQ9q9objH9rRgDLgs5c79
    
             Код необходимо распаковать в папку $GOPATH/src/coursera

             После распаковки у вас будет папка $GOPATH/src/coursera/basics

             Например, /home/rvasily/go/src/coursera/basics

### Задание к лекции

https://drive.google.com/open?id=1z15u9DNQd87YTa7yasauSwCdk6w7tvDF

### Описание лекции

Вот мы и добрались до основной темы курса. Начнём с азов - как запустить веб-сервер, как обрабатывать запросы, как получать данные запросов. Так же в этом занятии шаблоны и снятие метрик производительности с работающего веб-сервера.

### Материалы к лекции

Конечно же документация:

1. https://golang.org/pkg/net/http/

Дополнительные матералы

1. https://gowebexamples.github.io/ - примеры касательно разработки веба
2. https://golang.org/doc/articles/wiki/
3. https://astaxie.gitbooks.io/build-web-application-with-golang/
4. https://github.com/thewhitetulip/web-dev-golang-anti-textbook/
5. https://codegangsta.gitbooks.io/building-web-apps-with-go/content/
6. http://www.golangprograms.com/
7. http://marcio.io/2015/07/cheap-mapreduce-in-go/
8. https://www.rzaluska.com/blog/important-go-interfaces/
9. https://blog.cloudflare.com/the-complete-guide-to-golang-net-http-timeouts/ - про таймауты
10. http://polyglot.ninja/golang-making-http-requests/
11. http://tumregels.github.io/Network-Programming-with-Go/

На русском:

1. https://habrahabr.ru/post/330512/ - Многопользовательская игра на Go через telnet - чисто сеть
