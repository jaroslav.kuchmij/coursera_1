package main

import (
	"os"
	"io/ioutil"
	"log"
	"strconv"
	"fmt"
	"io"
)

const(
	symbol1 string = "├───"
	symbol2 string = "	"
	symbol3 string = "└───"
	symbol4 string = "│"
)

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
func dirTree(out io.Writer, path string, printFiles bool) error {
	var fullResult string
	var err error

	if printFiles {
		fullResult, err = readDirFileTrue(path, 0, 0)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		fullResult, err = readDirFileFalse(path, 0, 0)
		if err != nil {
			log.Fatal(err)
		}
	}

	fmt.Fprintln(out, deleteLastSymbol(fullResult))
	return nil
}

func readDirFileTrue(path string, countLvl int, countDir int) (string, error) {
	var fullResult string
	var fileName string

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return "", err
	}

	if countLvl == 1 {
		for _, file := range files {
			if file.IsDir() {
				countDir += 1
			}
		}
	}

	for i, file := range files {
		fileIsDir := file.IsDir()
		lenFiles := len(files)
		fileName = file.Name()
		if !fileIsDir {
			fileName = createNameFile(file.Name() , path )
		}

		line, count := getLineFileTrue(fileName, countLvl, countDir, lenFiles, i, fileIsDir)

		countDir = count
		fullResult += line

		if fileIsDir {
			newCount := countLvl + 1
			newPath := path + string(os.PathSeparator) + fileName
			newResalt, err := readDirFileTrue(newPath, newCount, countDir)
			if err != nil {
				return "", err
			}
			fullResult += newResalt
		}
	}
	return fullResult, nil
}

func getLineFileTrue(fileName string, countLvl int, countDir int, lenFiles int, i int, fileIsDir bool) (string, int)  {
	var line string
	switch countLvl {
	case 0:
		if lenFiles - 1 == i {
			line = symbol3 + fileName + "\n"
		} else {
			line = symbol1 + fileName + "\n"
		}
	case 1:
		if fileIsDir {
			countDir -= 1
		}
		if lenFiles - 1 == i {
			line = symbol4 + symbol2 + symbol3 + fileName + "\n"
		} else {
			line = symbol4 + symbol2 + symbol1 + fileName + "\n"
		}

	case 2:
		if lenFiles - 1 == i {
			if countDir == 0 {
				line = symbol4 + symbol2 + symbol2 + symbol3 + fileName + "\n"
			} else {
				line = symbol4 + symbol2 + symbol4 + symbol2 + symbol3 + fileName + "\n"
			}
		} else {
			if countDir == 0 {
				line = symbol4 + symbol2 + symbol2 + symbol1 + fileName + "\n"
			} else {
				line = symbol4 + symbol2 + symbol4 + symbol2 + symbol1 + fileName + "\n"
			}
		}

	case 3:
		if lenFiles - 1 == i {
			if countDir == 0 {
				line = symbol4 + symbol2 + symbol2 + symbol2 + symbol3 + fileName + "\n"
			} else {
				line = symbol4 + symbol2 + symbol4 + symbol2 + symbol2 + symbol3 + fileName + "\n"
			}
		} else {
			if countDir == 0 {
				line = symbol4 + symbol2 + symbol2 + symbol2 + symbol1 + fileName + "\n"
			} else {
				line = symbol4 + symbol2 + symbol4 + symbol2 + symbol2 + symbol1 + fileName + "\n"
			}
		}
	}

	return line, countDir
}

func readDirFileFalse(path string, countLvl int, prevQuantityDir int) (string, error) {
	var fullResult string
	var fileName string
	var lenDir int
	var i int

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return "", err
	}

	for _, file := range files {
		if file.IsDir() {
			lenDir += 1
		}

	}

	for _, file := range files {
		if file.IsDir(){
			fileName = file.Name()
			line := getLineFileFalse(fileName, countLvl, prevQuantityDir, lenDir, i)
			fullResult += line
			newCount := countLvl + 1

			newPath := path + string(os.PathSeparator) + fileName
			newResalt, err := readDirFileFalse(newPath, newCount, lenDir - i)
			if err != nil {
				return "", err
			}
			fullResult += newResalt

			i ++
		}
	}

	return fullResult, nil
}

func getLineFileFalse(fileName string, countLvl int, prevQuantityDir int, lenDir int, i int,) string {
	var line string
	switch countLvl {
	case 0:
		if lenDir - 1 == i {
			line = symbol3 + fileName + "\n"
		} else {
			line = symbol1 + fileName + "\n"
		}
	case 1:
		if lenDir - 1== i {
			if prevQuantityDir == 1 {
				line = symbol2 + symbol3 + fileName + "\n"
			} else {
				line = symbol4 + symbol2 + symbol3 + fileName + "\n" + symbol4
			}
		} else {
			line = symbol4 + symbol2 + symbol1 + fileName + "\n"
		}
	case 2:
		if lenDir - 1 == i {
			if prevQuantityDir == 1 {
				line = symbol2 + symbol2 + symbol3 + fileName + "\n"
			} else {
				line = symbol4 + symbol2 + symbol4 + symbol2 + symbol3 + fileName + "\n"
			}
		} else {
			line = symbol4 + symbol2 + symbol4 + symbol2 + symbol1 + fileName + "\n"
		}
	}
	return line
}

func createNameFile(name string, path string) string  {
	var newName string
	b, err := ioutil.ReadFile(path + string(os.PathSeparator) + name)
	if err != nil {
		panic(err)
	}
	if len(b) != 0 {
		newName = name + " (" + strconv.Itoa(len(b)) + "b" + ")"
	} else {
		newName = name + " (empty)"
	}
	return newName
}

func deleteLastSymbol(str string) string {
	return str[:len(str) - 1]
}
