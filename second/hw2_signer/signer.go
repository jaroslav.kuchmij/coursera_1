package main

import (
	"strconv"
	"context"
	"fmt"
	"time"
	"sort"
	"strings"
)

const (
	hashNum = 6
	quotaLimit = 1
)

// сюда писать код
func main()  {
	ExecutePipeline()

}

func ExecutePipeline(jobs... job)  {
	for _, job := range jobs {
		job := <- job.out
	}
	ctx, finish := context.WithTimeout(context.Background(), time.Second * 3)
	quotaCh := make(chan struct{}, quotaLimit)
	outSingleHash := make(chan string, MaxInputDataLen)
	inRes := make(chan string, MaxInputDataLen)
	outRes := make(chan string, 1)
	for i := 0; i < MaxInputDataLen; i++ {
		go SingleHash(ctx, i, outSingleHash, quotaCh)
	}

	go CombineResults(ctx, inRes, outRes)

	go func(ctx context.Context, out chan string) {
		for  {
			select {
			case val := <-out:
				fmt.Println("outSingleHash - ", val)
				go MultiHash(ctx, val, inRes)
			case <-ctx.Done():
				return
			}
		}
	}(ctx, outSingleHash)

LOOP:
	for  {
		select {
		case res := <-outRes:
			fmt.Println("RESULT - ", res)
			finish()
			break LOOP
		case <-ctx.Done():
			fmt.Println("Done:", ctx.Err())
			break LOOP
		}

	}

}

func SingleHash(ctx context.Context, data int, out chan string, quotaCh chan struct{})  {
	outVal1 := make(chan string)
	outVal2 := make(chan string)
	outVal3 := make(chan string)

	go func (th int, outVal1 chan string) {
		outVal1 <- DataSignerCrc32(strconv.Itoa(th))
	}(data, outVal1)
	go func(th int, outVal2 chan string, quotaCh chan struct{}) {
		quotaCh <- struct{}{}
		outVal2 <- DataSignerMd5(strconv.Itoa(th))
		<- quotaCh
	}(data, outVal2, quotaCh)
	go func(outVal2 chan string, outVal3 chan string) {
		for  {
			select {
			case val2 := <- outVal2:
				outVal3 <- DataSignerCrc32(val2)
				break
			}
		}

	}(outVal2, outVal3)
	for  {
		select {
		case val3 := <- outVal3:
			val1 := <- outVal1
			fmt.Println("SIngleHash: ", strconv.Itoa(data), " - Done")
			out <- val1 + "~" + val3
			val3 = ""
		case <-ctx.Done():
			return
		default:

		}
	}

	//val1 := DataSignerCrc32(data)
	//val2 := DataSignerCrc32(DataSignerMd5(data))
	//return val1 + "~" + val2
}

func MultiHash(ctx context.Context, data string, out chan string )  {
	outVal := make(chan string, 1)
	var res string
	var count int

	for i := 0; i < hashNum; i++ {
		go func (th int, outVal chan string) {
			outVal <- DataSignerCrc32(strconv.Itoa(th) + data)
		}(i, outVal)
		time.Sleep(time.Millisecond * 1)

	}

	for  {
		select {
		case val := <- outVal:
			res += val
			count++
			if count == hashNum {
				fmt.Println("MultiHash: ", data, " - Done")
				out <- res
				break
			}
		case <-ctx.Done():
			return
		}

	}
	//var val string
	//for i := 0; i < hashNum; i++ {
	//	val += DataSignerCrc32(strconv.Itoa(i) + data)
	//}
	//
	//return val
}

func CombineResults(ctx context.Context, in chan string, out chan string)  {
	res := []string{}
	for  {
		select {
		case val := <-in:
			res = append(res, val)
			if len(res) == MaxInputDataLen {
				sort.Strings(res)
				fmt.Println("CombineResults - Done")
				out <- strings.Join(res, "_")
			}
		case <-ctx.Done():
			return
		}
	}

	//sort.Strings(slice)
	//
	//return  strings.Join(slice, "_")
}

